# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2023, Inkscape Documentation Authors
# This file is distributed under the same license as the Inkscape Beginners'
# Guide package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Inkscape Beginners' Guide 1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-11 16:48+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: zh_TW\n"
"Language-Team: zh_TW <LL@li.org>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.1\n"

#: ../../source/objects-to-paths.rst:3
msgid "Editing Nodes on a Geometrical Shape"
msgstr ""

#: ../../source/objects-to-paths.rst:5
msgid ""
"There are multiple ways to modify an object in your drawing. If the "
"object is a rectangle, an ellipse, a star or a spiral, i.e. a geometrical"
" shape drawn with one of the shape tools, then this object is a "
"**shape**, and not a **path**."
msgstr ""

#: ../../source/objects-to-paths.rst:9
msgid ""
"Shapes can only be modified by dragging their specific handles. In "
"contrast to shapes, you will edit a path with tools that are meant for "
"modifying paths."
msgstr ""

#: ../../source/objects-to-paths.rst:12
msgid ""
"However, **shapes can be converted into paths**, so the path tools can "
"then be used with them."
msgstr ""

#: ../../source/objects-to-paths.rst:15
msgid "To convert a shape into a path:"
msgstr ""

#: ../../source/objects-to-paths.rst:17
msgid "Select the shape with the Selector tool."
msgstr ""

#: ../../source/objects-to-paths.rst:18
msgid "In the menu, select :menuselection:`Path --> Object to path`."
msgstr ""

#: ../../source/objects-to-paths.rst:21
msgid ""
"A shape can always be converted to a path, but a path can never be "
"converted back into an object!"
msgstr ""

